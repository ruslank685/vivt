<?php

namespace Database\Factories;

use App\Models\Employee;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class EmployeeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Employee::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->firstName,
            'surname' => $this->faker->lastName,
            'patronymic' => $this->faker->title,
            'email' => $this->faker->unique()->safeEmail,
            'phone' => $this->faker->numberBetween(89040000000, 89999999999),
            'num_card' => $this->faker->numberBetween(1001, 4000),
            'status' => $this->faker->numberBetween(0, 4),
        ];
    }
}
