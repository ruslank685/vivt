<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Как запустить

Для запуска проекта потребуется выполнить следующие действия

- Скачать проект;
- Установить на компьютер следующие библиотеки: PHP7.4 и выше, Composer 2.0, Node.js;
- Открыть проект в консоли;
- Прописать команду "npm i" + "npm run dev".
- Переименовать файл ".env.example" в ".env" и в нём прописать следующие настройки для подключения к MySql: DB_HOST=ruskondn.beget.tech
DB_PORT=3306
DB_DATABASE=ruskondn_vivt
DB_USERNAME=ruskondn_vivt
DB_PASSWORD=R6mwpLg1RZ.
- Завершающим этапом будет запуск проекта командой "php artisan serve";
- Проект будет доступен по адресу "localhost:8000".

## Помощь

- Ссылка на [документацию](https://laravel.com/docs) по фреймворку Laravel 8;
- Ссылка на [скачивание](https://windows.php.net/download#php-7.4) PHP 7.4;
- Ссылка на [скачивание](https://getcomposer.org/download/) Composer 2.0;
- Ссылка на [скачивание](https://nodejs.org/en/) Node.js.
- Ссылка на [скачивание](https://nibbleteam.ru/storage/app/media/ruskondn_vivt.sql.zip) базы данных.
- Тестовые данные аккаунта: 1. rus.konohov2016@yandex.ru 2. qwerty123456

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
