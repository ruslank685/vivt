<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ВИВТ – выполнение модуля web-приложения</title>

    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

{{--    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">--}}
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
</head>
<body>

<header class="header">
    <div class="header__container">

        <div class="header__left-side">
            <img src="{{ asset('images/logo.svg') }}" alt="" class="header__logo">
            <p class="header__logo-text">GazeSystem</p>
        </div>

        <div class="header__right-side">
            <nav class="header__menu">
                <a href="#">Главная</a>
                <a href="#">О нас</a>
                <a href="#">Контакты</a>
            </nav>

            <div class="header__items">

                @if (Route::has('login'))
                    <ul class="navbar-nav navbar-center">
                        @auth
                            <a href="{{ url('/dashboard') }}" class="header__btn header__btn_red">Панель управления</a>
                        @else
                            <a href="{{ route('login') }}" class="header__btn header__btn_blue">Войти</a>

                            @if (Route::has('register'))
                                <a href="{{ route('register') }}" class="header__btn header__btn_red">Зарегистрироваться</a>
                            @endif
                        @endauth
                    </ul>
                @endif


            </div>
        </div>
    </div>
</header>

<main>
    <section class="company">
        <div class="company__container">
            <div class="company__item">
                <p class="company__trend">Система управления</p>
                <img src="{{ asset('images/line.png') }}" class="company__img-line" alt="">
                <h1>Лучшая система контроля сотрудников</h1>
                <p class="company__desc">Мы предоставляем доступ к лучшей панели управления для контроля и учёта прохода сотрудников учреждения в здание и к своим рабочим местам.</p>
                <div class="company__items-btn">
                    <div class="company__btn-info">Подробнее</div>
                    {{-- <div class="company__btn-reg">Зарегистрироваться</div> --}}
                </div>
            </div>
            <div class="company__item">
                <lottie-player src="https://assets3.lottiefiles.com/packages/lf20_1vizLx.json" class="company__animation" background="transparent" speed="1" style="width: 45vw; height: 45vw;"  loop  autoplay></lottie-player>
            </div>
        </div>
    </section>

    <section class="advantages">
        <div class="advantages__container">
            <h2>Преимущества нашей технологии</h2>
            <p class="advantages__desc">Ниже приведены 3 пункта, которые определяют наше лидерство на рынке</p>
            <img src="{{ asset('images/line.png') }}" class="line" alt="">
            <div class="advantages__wrapper">

                <div class="advantages__item">
                    <div class="advantages__item-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-speedometer2" viewBox="0 0 16 16">
                            <path d="M8 4a.5.5 0 0 1 .5.5V6a.5.5 0 0 1-1 0V4.5A.5.5 0 0 1 8 4zM3.732 5.732a.5.5 0 0 1 .707 0l.915.914a.5.5 0 1 1-.708.708l-.914-.915a.5.5 0 0 1 0-.707zM2 10a.5.5 0 0 1 .5-.5h1.586a.5.5 0 0 1 0 1H2.5A.5.5 0 0 1 2 10zm9.5 0a.5.5 0 0 1 .5-.5h1.5a.5.5 0 0 1 0 1H12a.5.5 0 0 1-.5-.5zm.754-4.246a.389.389 0 0 0-.527-.02L7.547 9.31a.91.91 0 1 0 1.302 1.258l3.434-4.297a.389.389 0 0 0-.029-.518z"></path>
                            <path fill-rule="evenodd" d="M0 10a8 8 0 1 1 15.547 2.661c-.442 1.253-1.845 1.602-2.932 1.25C11.309 13.488 9.475 13 8 13c-1.474 0-3.31.488-4.615.911-1.087.352-2.49.003-2.932-1.25A7.988 7.988 0 0 1 0 10zm8-7a7 7 0 0 0-6.603 9.329c.203.575.923.876 1.68.63C4.397 12.533 6.358 12 8 12s3.604.532 4.923.96c.757.245 1.477-.056 1.68-.631A7 7 0 0 0 8 3z"></path>
                        </svg>
                    </div>
                    <p class="advantages__item-title">Скорость</p>
                    <p class="advantages__item-desc">
                        Система управления работает быстро благодаря такому мощному фреймворку, как Laravel.
                    </p>
                    <a href="" class="advantages__item-btn">Подробнее
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
                        </svg>
                    </a>
                </div>

                <div class="advantages__item">
                    <div class="advantages__item-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-emoji-heart-eyes" viewBox="0 0 16 16">
                            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path>
                            <path d="M11.315 10.014a.5.5 0 0 1 .548.736A4.498 4.498 0 0 1 7.965 13a4.498 4.498 0 0 1-3.898-2.25.5.5 0 0 1 .548-.736h.005l.017.005.067.015.252.055c.215.046.515.108.857.169.693.124 1.522.242 2.152.242.63 0 1.46-.118 2.152-.242a26.58 26.58 0 0 0 1.109-.224l.067-.015.017-.004.005-.002zM4.756 4.566c.763-1.424 4.02-.12.952 3.434-4.496-1.596-2.35-4.298-.952-3.434zm6.488 0c1.398-.864 3.544 1.838-.952 3.434-3.067-3.554.19-4.858.952-3.434z"></path>
                        </svg>
                    </div>
                    <p class="advantages__item-title">Удобство</p>
                    <p class="advantages__item-desc">
                        Панель управления поделена на блоки для удобного отображения данных.
                    </p>
                    <a href="" class="advantages__item-btn">Подробнее
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
                        </svg>
                    </a>
                </div>

                <div class="advantages__item">
                    <div class="advantages__item-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-circle-square" viewBox="0 0 16 16">
                            <path d="M0 6a6 6 0 1 1 12 0A6 6 0 0 1 0 6z"></path>
                            <path d="M12.93 5h1.57a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-9a.5.5 0 0 1-.5-.5v-1.57a6.953 6.953 0 0 1-1-.22v1.79A1.5 1.5 0 0 0 5.5 16h9a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 4h-1.79c.097.324.17.658.22 1z"></path>
                        </svg>
                    </div>
                    <p class="advantages__item-title">Простота</p>
                    <p class="advantages__item-desc">
                        Система управления сделана максимально удобно для использования даже новичками.
                    </p>
                    <a href="" class="advantages__item-btn">Подробнее
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
                        </svg>
                    </a>
                </div>

            </div>
        </div>
    </section>

    <section class="choose">
        <div class="choose__container">
            <div class="choose__item choose__item_l">
                <h2>Почему выбирают нас</h2>
                <p class="choose__text-desc">Это лучшая панель управления на рынке! Команда разработчиков постоянно выпускает новые функции для удобного и быстрого использования технологии.</p>
                <a href="#" class="choose__btn">Познакомиться поближе</a>
            </div>
            <div class="choose__item choose__item_r">
                <div class="choose__num">5 +</div>
                <div class="choose__text">Работников</div>

                <div class="choose__num">20 +</div>
                <div class="choose__text">Проектов</div>

                <div class="choose__num">430 +</div>
                <div class="choose__text">Клиентов</div>
            </div>
        </div>
    </section>

    <section class="feedback">
        <div class="feedback__container">
            <h2>Связаться с нами</h2>
            <form action="" class="feedback__form">
                <input type="text" name="form-name" class="feedback__input feedback__input_50" placeholder="Имя">

                <input type="text" name="form-surname" class="feedback__input feedback__input_50" placeholder="Фамилия">
                <input type="text" name="form-email" class="feedback__input feedback__input_100" placeholder="E-mail">
                <input type="text" name="form-company" class="feedback__input feedback__input_100" placeholder="Название кампании">
                <textarea name="" class="feedback__textarea feedback__input_100" placeholder="Ваше сообщение"></textarea>
                <button type="submit" class="feedback__btn">Отправить</button>
            </form>
        </div>
    </section>
</main>

<footer>
    <hr class="footer__hr" />
    <p class="footer__copyright">Copyright © {{ date('Y') }} Конохов Руслан</p>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>

    $(window).on("scroll", function() {
        if ($(window).scrollTop() > 50) {
            $('.header').addClass('header__scroll');
        } else {
            // $('.header').animate({height: "6vw"}, 300);
            $('.header').removeClass('header__scroll');
        }
    });


</script>

<style>
    * {
        margin: 0;
        padding: 0;
        font-family: 'Nunito', sans-serif;
    }

    body {
        background-color: #fcfcfc;
    }

    a {
        text-decoration: none;
    }

    .line {
        margin: 2vw auto;
        width: 10vw;
    }

    .header {
        top: 0;
        width: 100%;
        height: 6vw;
        display: flex;
        background-color: #fcfcfc;
        position: fixed;
        transition: all 1s;
        z-index: 10;
    }

    .header__scroll {
        height: 4vw;
        background-color: #fff;
        box-shadow: 0 .125rem .25rem rgba(0,0,0,.075);
        transition: all 1s;
    }

    .header__container {
        margin: auto;
        width: 90%;
        display: flex;
        justify-content: space-between;
    }

    .header__left-side,
    .header__right-side {
        display: flex;
    }

    .header__logo {
        margin: auto 0;
        height: 3vw;
        transition: all 1s;
    }

    .header__scroll .header__logo {
        height: 2vw;
        transition: all 1s;
    }

    .header__logo-text {
        margin: auto 0;
        margin-left: 2vw;
        font-size: 1.2vw;
        font-weight: 700;
        color: #101010;
    }

    .header__menu {
        margin: auto 0;
        display: flex;
    }

    .header__menu > a {
        margin-right: 2vw;
        font-size: 1.1vw;
        font-weight: 500;
        color: #101010;
    }

    .header__menu > a:last-of-type {
        margin-right: 0;
    }

    .header__items {
        margin: auto 0;
        margin-left: 2vw;
        display: flex;
    }

    .header__btn {
        padding: .5vw 1vw;
        width: auto;
        height: auto;
        font-size: 1vw;
        font-weight: 500;
        color: #101010;
        border-radius: 5px;
        transition: all 1s;
    }

    .header__btn:hover {
        cursor: pointer;
        transition: all 1s;
    }

    .header__btn_red {
        background-color: #fb7179;
        border: 1px solid #fb7179;
        color: #fff;
    }

    .header__btn_red:hover {
        background-color: unset;
        color: #fb7179;
    }

    .header__btn_blue {
        background-color: #0d6efd;
        border: 1px solid #0d6efd;
        color: #fff;
    }

    .company {
        margin-top: 6vw;
        min-height: calc(100vh - 6vw);
        display: flex;
        background-image: url("../images/bg-2.png");
        background-position: center;
        background-repeat: no-repeat;
        background-size: 100%;
    }

    .company__container {
        margin: 0 auto;
        width: 90%;
        display: flex;
    }

    .company__item {
        width: calc(50% - 1vw);
        display: flex;
        flex-direction: column;
        justify-content: center;
    }

    .company__trend {
        font-size: 1vw;
        font-weight: 300;
        color: #10101090;
    }

    .company__img-line {
        width: 10vw;
    }

    .company h1 {
        margin-top: 2vw;
        font-size: 3vw;
        font-weight: 600;
        color: #101010;
        line-height: 3vw;
    }

    .company__desc {
        margin-top: 2vw;
        font-size: 1.2vw;
        font-weight: 500;
        color: #101010da;
    }

    .company__items-btn {
        margin-top: 2vw;
        display: flex;
        justify-content: flex-start;
    }

    .company__btn-info {
        margin: auto 0;
        padding: .6vw 2vw;
        width: auto;
        height: auto;
        font-size: 1vw;
        font-weight: 500;
        color: #fff;
        background-color: #1ea59a;
        border: 1px solid #1ea59a;
        border-radius: 5px;
        display: block;
    }

    .company__btn-reg {
        margin: auto 0;
        margin-left: 2vw;
        font-size: 1vw;
        font-weight: 500;
        color: #0d6efd;
    }

    .company__animation {
        margin: 0 auto;
    }

    .advantages {
        margin: 4vw 0;
        margin-bottom: 0;
        padding: 4vw 0;
        width: 100%;
        min-height: 40vw;
        display: flex;
        background-color: #f8f8f8;
        border-top-left-radius: 50%;
    }

    .advantages__container {
        margin: 0 auto;
        width: 90%;
        display: flex;
        flex-direction: column;
    }

    .advantages h2 {
        margin: 0 auto;
        width: 60%;
        font-size: 2vw;
        font-weight: 600;
        color: #101010;
        text-align: center;
    }

    .advantages__desc {
        margin: 0 auto;
        margin-top: 1vw;
        width: 60%;
        font-size: 1.2vw;
        font-weight: 500;
        color: #101010da;
        text-align: center;
    }

    .advantages__wrapper {
        display: flex;
        justify-content: space-between;
    }

    .advantages__item {
        margin: 0;
        padding: 2vw 0;
        width: 28vw;
        min-height: 16vw;
        height: auto;
        background-color: #fff;
        border-radius: 10px;
        -webkit-box-shadow: 0px 0px 30px -10px #10101020;
        -moz-box-shadow: 0px 0px 30px -10px #10101020;
        box-shadow: 0px 0px 30px -10px #10101020;
        display: flex;
        flex-direction: column;
        transition: all 1s;
    }

    .advantages__item:hover {
        background-color: unset;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
        border: 1px solid #0d6efd;
        transition: all 1s;
    }

    .advantages__item-icon {
        margin-left: 2vw;
        margin-right: 2vw;
        width: 4vw;
        height: 4vw;
        background-color: rgba(30, 165, 154, 0.1);
        border-radius: 10px;
        display: flex;
        transition: all 1s;
    }

    .advantages__item:hover .advantages__item-icon {
        background-color: #fff;
        transition: all 1s;
    }

    .advantages__item-icon > * {
        margin: auto;
    }

    .advantages__item-title {
        margin: 1vw auto;
        width: calc(100% - 4vw);
        font-size: 1.5vw;
        font-weight: 600;
        color: #101010;
    }

    .advantages__item-desc {
        margin: 0 auto;
        width: calc(100% - 4vw);
        font-size: 1vw;
        font-weight: 500;
        color: #101010da;
    }

    .advantages__item-btn {
        margin: 0 auto;
        margin-top: 1vw;
        width: calc(100% - 4vw);
        font-size: 1vw;
        font-weight: 500;
        color: #0d6efd;
        display: flex;
        transition: all 1s;
    }

    .advantages__item-btn > * {
        margin: auto 0;
    }

    .advantages__item-btn > svg {
        margin-left: .5vw;
        transition: all 1s;
    }

    .advantages__item-btn:hover svg {
        margin-left: 2vw;
    }

    .choose {
        padding: 4vw 0;
        width: 100%;
        height: auto;
        display: flex;
        background-color: #f8f8f8;
        border-bottom-right-radius: 50%;
    }

    .choose__container {
        margin: 0 auto;
        width: 90%;
        display: flex;
        justify-content: space-between;
    }

    .choose__item {
        height: auto;
        width: 45%;
    }

    .choose h2 {
        margin-bottom: 2vw;
        font-size: 1.5vw;
        font-weight: 500;
        color: #101010;
    }

    .choose__text-desc {
        margin-bottom: 2vw;
        font-size: 1.1vw;
        font-weight: 300;
        color: #10101090;
    }

    .choose__btn {
        padding: .5vw 1vw;
        font-size: 1.1vw;
        font-weight: 300;
        color: #0d6efd;
        border: 1px solid #0d6efd;
        border-radius: 5px;
        display: inline-block;
        transition: all 1s;
    }

    .choose__btn:hover {
        padding: .5vw 1.5vw;
        color: #fff;
        background-color: #0d6efd;
        transition: all 1s;
    }

    .choose__num {
        font-size: 1.5vw;
        font-weight: 500;
        color: #0d6efd;
        text-align: center;
    }

    .choose__text {
        margin-bottom: 1vw;
        font-size: 1vw;
        font-weight: 500;
        color: #10101090;
        text-transform: uppercase;
        text-align: center;
    }

    .choose__text:last-of-type {
    margin-bottom: 0;
    }

    .feedback {
        margin: 4vw 0;
        width: 100%;
        display: flex;
        background-image: url("../images/bg-5.png");
        background-position: center;
        background-repeat: no-repeat;
        background-size: 80%;
    }

    .feedback__container {
        margin: 0 auto;
        width: 90%;
    }

    .feedback h2 {
        margin-bottom: 2vw;
        font-size: 1.5vw;
        font-weight: 600;
        color: #101010;
        text-align: center;
    }

    .feedback__form {
        margin: 0 auto;
        width: 60%;
        display: flex;
        justify-content: space-between;
        flex-wrap: wrap;
    }

    input, textarea {
        outline: none;
    }

    .feedback__input {
        height: 3vw;
        margin-bottom: .5vw;
        border: 1px solid #0d6efd90;
        border-radius: 5px;
        transition: all 1s;
    }

    .feedback__input:hover {
        border: 1px solid #0d6efd;
        transition: all 1s;
    }

    .feedback__textarea {
        height: 8vw;
        border: 1px solid #0d6efd90;
        border-radius: 5px;
    }

    .feedback__input_50 {
        width: calc(50% - .25vw);
    }

    .feedback__input_100 {
        width: 100%;
    }

    .feedback__btn {
        margin-top: 2vw;
        padding: .5vw 1vw;
        background-color: #fb7179;
        color: #fff;
        border: 1px solid #fb7179;
        border-radius: 5px;
        display: inline-block;
        transition: all 1s;
    }

    .feedback__btn:hover {
        background-color: #ffffff00;
        border-color: #fb717900;
        color: #fb7179;
        box-shadow: 4px 4px 15px -2px rgba(16, 16, 16, 0.2);
    }

    footer {
        margin-top: 4vw;
        padding: 2vw 0;
        width: 100%;
        height: auto;
        background-color: #f8f8f8;
    }

    .footer__hr {
        margin: 0 auto;
        width: 90%;
        height: 1px;
        border: none;
        background-color: #10101030;
    }

    .footer__copyright {
        margin-top: 1vw;
        text-align: center;
        font-size: 1vw;
        font-weight: 500;
        color: #101010da;
    }

</style>

</body>
</html>
