<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="mb-4 p-6 bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <h6 class="border-bottom pb-2 mb-4">Работники  учреждения</h6>
                <table class="table table-striped table-sm">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>ФИО</th>
                        <th>E-mail</th>
                        <th>Телефон</th>
                        <th>№ карты</th>
                        <th>Статус</th>
                      </tr>
                    </thead>
                    <tbody>
        
                      <tr>
                        <td>1</td>
                        <td>Конохов Руслан Юрьевич</td>
                        <td>rus.konohov2016@yandex.ru</td>
                        <td>8 (800) 000-00-00</td>
                        <td>1001</td>
                        <td>В здании</td>
                      </tr>

                      <tr>
                        <td>2</td>
                        <td>Конохов Руслан Юрьевич</td>
                        <td>rus.konohov2016@yandex.ru</td>
                        <td>8 (800) 000-00-00</td>
                        <td>1002</td>
                        <td>В кабинете №110</td>
                      </tr>

                      <tr>
                        <td>3</td>
                        <td>Конохов Руслан Юрьевич</td>
                        <td>rus.konohov2016@yandex.ru</td>
                        <td>8 (800) 000-00-00</td>
                        <td>1002</td>
                        <td>Вне здания</td>
                      </tr>

                    </tbody>
                  </table>
            </div>

            <div class="p-6 bg-white overflow-hidden shadow-xl sm:rounded-lg">
                {{-- <x-jet-welcome /> --}}
                
                <h6 class="border-bottom pb-2 mb-4">Последние обновления</h6>

                <div class="d-flex text-muted pt-3">
                    <svg class="bd-placeholder-img flex-shrink-0 me-2 rounded" width="32" height="32" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: 32x32" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#007bff"></rect><text x="50%" y="50%" fill="#007bff" dy=".3em">32x32</text></svg>
              
                    <p class="pb-3 mb-0 small lh-sm border-bottom">
                      <strong class="d-block text-gray-dark">@ruslan</strong>
                      Some representative placeholder content, with some information about this user. Imagine this being some sort of status update, perhaps?
                    </p>
                </div>

            </div>
        </div>
    </div>
</x-app-layout>
